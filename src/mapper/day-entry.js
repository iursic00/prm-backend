function MapDbDayEntriesArrayModelToViewModel(dbModel) {
    return new Promise( (resolve) => {
        const viewModel = dbModel.map( dayEntry => {
            const temp = {};
            temp.id = dayEntry.id;
            temp.hours = dayEntry.hours;
            temp.notes = dayEntry.notes.toString().trim();
            temp.name = `${dayEntry.user.firstName} ${dayEntry.user.lastName}`;
            return temp;
        });

        resolve(viewModel);
    });
}

export default {MapDbDayEntriesArrayModelToViewModel};