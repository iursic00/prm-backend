import OAuth2Strategy from 'passport-oauth2';
import passport from 'passport';
import request from 'restler';
import jwt from 'jsonwebtoken';
import serverConfiguration from '../configuration/server';
import harvestConf from '../configuration/harvest';

const env = process.env.NODE_ENV || "development";
const harvestParameters = harvestConf[env];


passport.use(new OAuth2Strategy.Strategy({
        authorizationURL: 'https://profico.harvestapp.com/oauth2/authorize',
        tokenURL: 'https://profico.harvestapp.com/oauth2/token',
        clientID: harvestParameters.clientID,
        clientSecret: harvestParameters.clientSecret,
        callbackURL: `${serverConfiguration.redirectionRootUrl()}/login`
    },
    function(accessToken, refreshToken, profile, cb) {
        const options = {};
        options.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Credentials':'*'
        };

        request.get(`https://profico.harvestapp.com/account/who_am_i?access_token=${accessToken}`, options).on('complete',(result) => {
            if (result instanceof Error) {
                cb(result);
            } else {
                if(result.user.admin) {
                    const user = {
                        id: result.user.id,
                        email: result.user.email,
                        firstName: result.user.first_name,
                        lastName: result.user.last_name,
                        avatarUrl: result.user.avatar_url,
                        isAdmin: true
                    };

                    const token = jwt.sign({
                        id: user.id,
                        accessToken: accessToken,
                        refreshToken: refreshToken,
                        isAdmin: true
                    }, serverConfiguration.jsonWebToken.secret,{expiresIn:serverConfiguration.jsonWebToken.expires});

                    user.token = token;
                    cb(null,user);
                } else {
                    cb(null, {success: false, message: "You must have admin role"});
                }
            }
        });

    }
));

export default passport;