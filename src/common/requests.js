import request from 'restler';
import Promise from 'bluebird';

const _options = {
    headers:{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
};

function Get(url) {
    return new Promise(function(resolve, reject) {
        request.get(url, _options).on('complete', function (response) {
            if (response instanceof Error || response.error) {
                reject(response);
            } else {
                resolve(response);
            }
        });
    });
}

export default {Get};