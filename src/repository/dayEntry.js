import db from '../models';

class DayEntryRepository {

    CreateMany(dayEntry) {
        return db.dayEntry.bulkCreate(dayEntry);
    }

    GetByTaskIdAndPayPeriodId(payPeriodId, taskId) {
        return db.dayEntry.findAll({
            where: {
                taskId: taskId,
                payPeriodId: payPeriodId
            }
            ,include:[
                {model: db.user}
            ]
        });
    }

    DeleteByPayPeriodId(payPeriodId) {
        return db.dayEntry.destroy({
            where: {
                payPeriodId: payPeriodId
            }
        });
    }
}

export default new DayEntryRepository();