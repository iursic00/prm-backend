import winston from 'winston';
import loggerConfig from '../configuration/logger';

function createLogger() {
    const transports = [];
    winston.addColors(loggerConfig.colors);
    winston.setLevels(loggerConfig.levels);

    if(loggerConfig.logconsole.enable) {
        transports.push(new winston.transports.Console({
            level: loggerConfig.logconsole.level,
            levels: loggerConfig.levels,
            colorize: true
        }));
    }

    const logger = new winston.Logger ({
        level: loggerConfig.level,
        levels: loggerConfig.levels,
        transports: transports,
    });

    return logger;
}

export default createLogger;