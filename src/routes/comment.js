import {Router} from 'express';
import CommentController from '../controller/comment';
import httpStatus from 'http-status-codes';

const router = Router();

router.post('/', (req, res, next) => {

    if(!req.body) {
        res.status(httpStatus.NO_CONTENT).json({
            error: "Body is required"
        });
        return;
    }

    if( !req.body.taskAssignmentId || !req.body.commentText) {
        next();
        return;
    }

    CommentController.Create(req.body.payPeriodId, req.body.taskAssignmentId, req.body.taskAssignmentName, req.body.commentText, req.decoded.isAdmin)
        .then( comment => {
            res.status(httpStatus.OK).json({
                clients: comment
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error:error
            });
        });
});

router.all('*', (req,res) => {
    res.status(httpStatus.NOT_FOUND).json({
        message: "That route doesn't exist or parameters missing"
    });
});

export default router;
