import clientRoutes from './client';
import projectRoutes from './project';
import payPeriodRoutes from './payPeriod';
import syncRoutes from './sync';
import dayEntryRoutes from './day-entry';
import commentsRoutes from './comment';
import {Router} from 'express';

const router = Router();

router.use('/client', clientRoutes);
router.use('/project', projectRoutes);
router.use('/payPeriod', payPeriodRoutes);
router.use('/sync', syncRoutes);
router.use('/day-entry', dayEntryRoutes);
router.use('/comment', commentsRoutes);

export default router;