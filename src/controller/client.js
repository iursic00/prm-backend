import harvestService from '../service/harvest';

class ClientController {
    getClients(token) {
        return harvestService.client.GetAll(token);
    }
}

export default new ClientController();