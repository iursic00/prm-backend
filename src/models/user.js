
export default function(sequelize, DataTypes) {
    const User = sequelize.define("user",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            harvestId: {type: DataTypes.INTEGER, allowNull:false, unique: true},
            email: {type: DataTypes.STRING, allowNull: true},
            isAdmin: {type: DataTypes.BOOLEAN, allowNull: false},
            isProjectManager: {type: DataTypes.BOOLEAN, allowNull: false},
            firstName: {type: DataTypes.STRING, allowNull: false},
            lastName: {type: DataTypes.STRING, allowNull: false},
            isContractor: {type: DataTypes.BOOLEAN, allowNull: false},
            isActive: {type: DataTypes.BOOLEAN, allowNull: false},
            harvestUpdatedAt: {type: DataTypes.DATE, allowNull: true}
        },
        {
            indexes: [
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    User.associate = function(models) {
        User.dayEntries = User.hasMany( models.dayEntry, {foreignKey: 'userId', sourceKey: 'harvestId'});
    };

    return User;
}
