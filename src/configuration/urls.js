const rootUrl = "https://profico.harvestapp.com/";

const client = {
    getAll: function (accessToken) {
        return `${rootUrl}clients?access_token=${accessToken}`;
    },

    getAllContactsForClient: function(clientId, accessToken) {
        return `${rootUrl}clients/${clientId}/contacts?access_token=${accessToken}`;
    }
};

const project = {
    getByClientId: function(clientId, accessToken) {
        return `${rootUrl}projects?client=${clientId}&access_token=${accessToken}`;
    },

    getById: function (projectId, accessToken) {
        return `${rootUrl}projects/${projectId}?access_token=${accessToken}`;
    }
};

const entries = {
    getByProjectIdForPeriod: function(projectId, startDate, endDate, accessToken) {
        return `${rootUrl}projects/${projectId}/entries?from=${startDate}&to=${endDate}&access_token=${accessToken}`;
    }
};

const task = {
    getById: function(taskId, accessToken) {
        return `${rootUrl}tasks/${taskId}?access_token=${accessToken}`;
    },

    getByProjectId: function(projectId, accessToken) {
        return `${rootUrl}projects/${projectId}/task_assignments?access_token=${accessToken}`;
    },

    getAll: function(accessToken) {
        return `${rootUrl}tasks?access_token=${accessToken}`;
    }
};

const user = {
    getById: function(userId, accessToken) {
        return `${rootUrl}people/${userId}?access_token=${accessToken}`;
    },

    getAll: function(accessToken) {
        return `${rootUrl}people?access_token=${accessToken}`;
    }

};

export default {client, project, entries, task, user};