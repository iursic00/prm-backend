import {Router} from 'express';
import ClientController from '../controller/client';
import httpStatus from 'http-status-codes';

const router = Router();

router.get('/', (req, res) => {
    ClientController.getClients(req.decoded.accessToken)
        .then( clients => {
            res.status(httpStatus.OK).json({
                clients: clients
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            error:error
            });
        });
});
export default router;
