import {Router} from 'express';
import refreshTasks from '../service/tasks/refreshTasks';
import refreshUsers from '../service/user/refreshUsers';
import httpStatus from 'http-status-codes';

const router = Router();

router.get('/tasks', (req, res) => {

    refreshTasks(req.decoded.accessToken)
        .then( created => {
            res.status(httpStatus.OK).json({
                data: created
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});

router.get('/users', (req, res) => {

    refreshUsers(req.decoded.accessToken)
        .then( created => {
            res.status(httpStatus.OK).json({
                data: created
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});

export default router;