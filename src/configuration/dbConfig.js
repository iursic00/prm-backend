export default{
    development: {
        database: "prm_development",
        username: "prm",
        password: "73db3b66e14b246901959e81148461d3",
        options: {
            host: 'localhost',
            dialect: 'postgres',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    },

    production: {
        database: "prm_production",
        username: "postgres",
        password: "1234",
        options: {
            host: 'localhost',
            dialect: 'postgres',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    }
};