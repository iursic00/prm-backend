import db from '../models';
import moment from 'moment';

class PayPeriodRepository {

    CreatePayPeriod(model) {
        return db.payPeriod.create(model);
    }

    GetAll() {
        return db.payPeriod.findAll();
    }

    GetAllForYear(year) {
        return db.payPeriod.findAll(
            {
                where: {
                    startDate: {
                        gte: moment().year(year).startOf('year').toDate(),
                        lt: moment().year(year + 1).startOf('year').toDate()
                    }
                },

                order: [['createdAt', 'DESC']]

            });
    }

    GetPayPeriod(id) {
        return db.payPeriod.findOne({
            where: {id: id},
            include: [{
                model: db.taskAssignment,
                include: [{
                    model: db.task,
                    include: [{model: db.dayEntry, where: { payPeriodId: id}, required: false}]
                }, { model: db.comment}]
            }]
        });
    }

    GetById(id) {
        return db.payPeriod.findOne({where:{id: id}});
    }

    GetPayPeriodByProjectIdInTimeRange(projectId, startDate, endDate) {

        return db.payPeriod.findAll({
            where: {
                projectId: projectId,
                $or: [
                    {startDate: {gte: moment(startDate).toDate(), lt: moment(endDate).toDate()}},
                    {endDate: {gte: moment(startDate).toDate(), lt: moment(endDate).toDate()}}
                ]
            }});
    }

    GetFromLinkParameters(payPeriodId, projectId, clientId) {
        return db.payPeriod.findOne({
            where: {
                id: payPeriodId,
                projectId: projectId,
                clientId: clientId
            }
        });
    }
}

export default new PayPeriodRepository();