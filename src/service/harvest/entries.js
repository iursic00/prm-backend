import request from '../../common/requests';
import harvestUrls from '../../configuration/urls';

function GetByProjectIdForPeriod(projectId, startDate, endDate, accessToken) {
    return request.Get(harvestUrls.entries.getByProjectIdForPeriod(projectId, startDate, endDate, accessToken));
}

export default {
    GetByProjectIdForPeriod
};