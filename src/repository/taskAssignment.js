import db from '../models';

class TaskAssignmentRepository {

    CreateMany(taskAssignmetns) {
        return db.taskAssignment.bulkCreate(taskAssignmetns);
    }

    UpdateOne(taskAssignment) {
        return db.taskAssignment.update(taskAssignment, {where: {id: taskAssignment.id}});
    }

    DeleteByPayPeriodId(payPeriodId) {
        return db.taskAssignment.destroy({
            where: {
                payPeriodId: payPeriodId
            }
        });
    }

    GetByPayPeriodId(payPeriodId) {
        return db.taskAssignment.findAll({
            where: {
                payPeriodId: payPeriodId
            }
        });
    }
}

export default new TaskAssignmentRepository();
