
export default function(sequelize, DataTypes) {
    const Task = sequelize.define("task",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            harvestId: {type: DataTypes.INTEGER, allowNull:false, unique: true},
            name: {type: DataTypes.STRING, allowNull: false},
            billableByDefault: {type: DataTypes.BOOLEAN, allowNull:false},
            harvestUpdatedAt: {type: DataTypes.DATE, allowNull: false},
            isDefault: {type: DataTypes.BOOLEAN, allowNull: false},
            defaultHourlyRate: {type: DataTypes.DECIMAL, allowNull: true},
            deactivated: {type: DataTypes.BOOLEAN, allowNull: false}
        },
        {
            indexes: [
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    Task.associate = function(models) {
        Task.hasMany(models.taskAssignment, {foreignKey: 'taskId', sourceKey: 'harvestId'});
        Task.dayEntries = Task.hasMany(models.dayEntry, {foreignKey: 'taskId', sourceKey: 'harvestId'});
    };

    return Task;
}
