import PayPeriodRepository from './payPeriod';
import TaskRepository from './task';
import TaskAssignment from './taskAssignment';
import DayEntryRepository from './dayEntry';
import UserRepository from './user';
import CommentRepository from './comment';

export default {
    payPeriod: PayPeriodRepository,
    task: TaskRepository,
    taskAssignment:  TaskAssignment,
    dayEntry: DayEntryRepository,
    user: UserRepository,
    comment: CommentRepository
};