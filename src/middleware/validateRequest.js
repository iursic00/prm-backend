/* eslint-disable no-console */
import httpStatus from 'http-status-codes';
import jwt from 'jsonwebtoken';
import serverConfigration from '../configuration/server';

export default function (req, res, next) {
    if(req.method == "OPTIONS")
        next();

    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if(token) {
        jwt.verify(token, serverConfigration.jsonWebToken.secret, function(err, decoded) {
            if (err) {
                return res.status(httpStatus.FORBIDDEN).json({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(httpStatus.FORBIDDEN).send({
            success: false,
            message: 'No token provided.'
        });
    }
}