import harvestService from '../service/harvest';
import moment from 'moment';

class Report {

    CreateReport(clientId, projectId, startDate, endDate, accessToken) {
        startDate = moment(startDate).format('YYYYMMDD');
        endDate = moment(endDate).format('YYYYMMDD');
        return harvestService.project.GetById(projectId, accessToken);
    }
}

export default new Report();