import request from '../../common/requests';
import harvestUrls from '../../configuration/urls';

function GetAll(accessToken) {
    return request.Get(harvestUrls.client.getAll(accessToken));
}

function GetContactsForClient(clientId, accessToken) {
    return request.Get(harvestUrls.client.getAllContactsForClient(clientId, accessToken));
}

export default {GetAll, GetContactsForClient};