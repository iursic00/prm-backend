import repo from '../repository';
import Enums from '../configuration/enums';
import slackService from '../service/slack/sendMessage';
import moment from 'moment';

class CommentController {

    Create(payPeriodId, taskAssignmentId, taskAssignmentName, commentText, isAdmin) {
        const comment = {
            taskAssignmentId: taskAssignmentId,
            commentText: commentText,
            commentBy: isAdmin ? Enums.Roles.Company.value : Enums.Roles.Client.value
        };

        if(isAdmin) {
            return repo.comment.Create(comment);
        } else {
            return repo.comment.Create(comment)
                .then(() => {
                    return repo.payPeriod.GetById(payPeriodId);
                })
                .then( (payPeriod) => {
                    return slackService.SendMessage(`${payPeriod.name} WH report for period ${moment(payPeriod.startDate).format('DD.MM.YYYY. HH:mm')} - ${moment(payPeriod.endDate).format('DD.MM.YYYY. HH:mm')} was commented( Task asssignment name: ${taskAssignmentName})`);
                });
        }
    }
}

export default new CommentController();