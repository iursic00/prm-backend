module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name         : "prm-backend",
      script       : "dist/server.js",
      ignore_watch : ["node_modules/**", "logs/**"],
      env_production : {
        NODE_ENV: "production"
      }
    }
  ]
};
