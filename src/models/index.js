import Sequelize from 'sequelize';
import dbConfig from '../configuration/dbConfig';
import fs from 'fs';
import path from 'path';

const env = process.env.NODE_ENV || "development";
const config = dbConfig[env];
const sequelize = new Sequelize(config.database, config.username, config.password, config.options);

const db = {};

fs.readdirSync(__dirname)
    .filter(function(file) {
        return file.indexOf(".") !== 0 && file !== "index.js" && !file.endsWith(".map");
    })
    .forEach(function(file) {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;