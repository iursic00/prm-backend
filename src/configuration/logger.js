export default{
    level: 'debug',
    levels:{
        debug: 4,
        info: 3,
        warn: 2,
        error: 1,
        fatal: 0,
    },
    colors:{
        debug: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red',
        fatal: 'red bold'
    },
    logconsole:{
        enable: true,
        level: 'debug'
    }
};