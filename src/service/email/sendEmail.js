import gmailSend from 'gmail-send';
import emailConfig from '../../configuration/email';
import Promise from 'bluebird';


export default function (to, subject, content) {

    const mailOptions = emailConfig.mailOptions;
    mailOptions.to = to;
    mailOptions.subject = subject;
    mailOptions.html = content;
    const send = gmailSend(mailOptions);

    return new Promise( (resolve, reject) => {
        send(mailOptions, (error, info) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(`Message ${info.messageId} sent: ${info.response}`);
        });
    });
}
