import request from '../../common/requests';
import harvestUrls from '../../configuration/urls';

function GetByClientId(clientId, accessToken) {
    return request.Get(harvestUrls.project.getByClientId(clientId, accessToken));
}

function GetById(projectId, accessToken) {
    return request.Get(harvestUrls.project.getById(projectId, accessToken));
}


export default {GetByClientId, GetById};