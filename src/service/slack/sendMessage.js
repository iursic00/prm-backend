import request from 'restler';
import Promise from 'bluebird';

const _options = {
    headers:{
        'Content-Type': 'text/html',
        'Accept': 'text/html'
    }
};

const url = "https://profico.slack.com/services/hooks/slackbot?token=92PxODg2vLrEJPJMtibRCSMY&channel=%23prm";

function SendMessage(message) {
    _options.data = message;
    return new Promise(function(resolve, reject) {
        request.post(url, _options).on('complete', function (response) {
            if (response instanceof Error || response.error) {
                reject(response);
            } else {
                resolve(response);
            }
        });
    });
}

export default {SendMessage};