
export default function(sequelize, DataTypes) {
    const Comment = sequelize.define("comment",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            commentText: {type: DataTypes.TEXT, allowNull: false},
            commentBy: {type: DataTypes.INTEGER, allowNull: false}
        },
        {
            indexes:[
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    Comment.associate = function(models) {
        Comment.belongsTo( models.taskAssignment, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull:false
            }
        });

    };

    return Comment;
}