import db from '../models';

class UserRepository {

    CreateMany(users) {
        return db.user.bulkCreate(users);
    }

    GetAll() {
        return db.user.findAll();
    }
}

export default new UserRepository();