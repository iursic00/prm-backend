import client from './client';
import project from './project';
import entries from './entries';
import task from './task';
import user from './user';

export default{
    client: client,
    project: project,
    entries: entries,
    task: task,
    user: user
};
