FROM node
RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/dist
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
EXPOSE 2907
CMD [ "npm", "run" , "build" ]
CMD [ "npm", "run" , "serve" ]