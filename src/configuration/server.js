export default{
    port: 2907,
    jsonWebToken: {
        secret: "ohedoghvow5hegohw45",
        expires: "18h"
    },
    redirectionRootUrl: function() {
        const env = process.env.NODE_ENV || "development";
        if(env == 'development') {
            return "http://127.0.0.1:2907";
        } else {
            return "http://prmapi.proficodev.com";
        }
    },

    frontendRedirectionUrl: function() {
        const env = process.env.NODE_ENV || "development";
        if(env == 'development') {
            return "http://127.0.0.1:4200";
        } else {
            return "http://prm.proficodev.com";
        }
    }
};