
export default function(sequelize, DataTypes) {
    const TaskAssignment = sequelize.define("taskAssignment",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            hourlyRate: {type: DataTypes.DECIMAL, allowNull: true},
            harvestId: {type: DataTypes.INTEGER, allowNull: false},
            billable: {type: DataTypes.BOOLEAN, allowNull: false},
            taskId: {type: DataTypes.INTEGER, allowNull: false},
            description: {type: DataTypes.TEXT, allowNull: true},
            billableHours: {type: DataTypes.DECIMAL, allowNull: true},
            unBillableHours: {type: DataTypes.DECIMAL, allowNull: true}
        },
        {
            indexes:[
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    TaskAssignment.associate = function(models) {
        TaskAssignment.belongsTo( models.payPeriod, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull:false
            }
        });

        TaskAssignment.belongsTo( models.task, {
            onDelete: "CASCADE",
            foreignKey: 'taskId',
            targetKey: 'harvestId'
        });

        TaskAssignment.comments = TaskAssignment.hasMany(models.comment);

    };

    return TaskAssignment;
}