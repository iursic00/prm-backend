import db from '../../models';

function createPayPeriodModel (data, startDate, endDate, projectId, clientId) {

    const dbModel = {
        name: data.project.name,
        startDate: startDate,
        endDate: endDate,
        status: 1,
        price: data.project.budget,
        projectId: projectId,
        clientId: clientId
    };
    return dbModel;
}

export default function PrepareAndCreateProject(data, startDate, endDate, projectId, clientId) {
    const payPeriodDbModel = createPayPeriodModel(data[0], startDate, endDate, projectId, clientId);
    let payPeriodId = 0;

    return db.sequelize.transaction( (t) => {
        return db.payPeriod.create(payPeriodDbModel, {transaction : t})
            .then( (payPeriod) => {
                payPeriodId = payPeriod.id;
                const dbModel = data[2].map( task => {
                    task = task.task_assignment;
                    const tempObject = {};
                    tempObject.harvestId = task.id;
                    tempObject.hourlyRate = task.hourly_rate;
                    tempObject.billable = task.billable;
                    tempObject.taskId = task.task_id;
                    tempObject.payPeriodId = payPeriod.dataValues.id;
                    return tempObject;
                });
                return db.taskAssignment.bulkCreate(dbModel, {transaction: t});
            })
            .then( () => {
                const dayEntries = data[1].map( dayEntry => {
                    dayEntry = dayEntry.day_entry;
                    const tempObject = {};
                    tempObject.harvestId = dayEntry.id;
                    tempObject.notes = dayEntry.notes;
                    tempObject.hours = dayEntry.hours;
                    tempObject.userId = dayEntry.user_id;
                    tempObject.projectId = dayEntry.project_id;
                    tempObject.taskId = dayEntry.task_id;
                    tempObject.harvestUpdatedAt = dayEntry.updated_at;
                    tempObject.payPeriodId = payPeriodId;
                    return tempObject;
                });
                return db.dayEntry.bulkCreate(dayEntries, {transaction: t});
            })
            .then( () => {
                payPeriodDbModel.id = payPeriodId;
                return new Promise( (resolve) => {
                    resolve(payPeriodDbModel);
                });
            })
    });
    /*
    return repo.payPeriod.CreatePayPeriod(payPeriodDbModel)
        .then( payPeriod => {
            payPeriodId = payPeriod.id;
            const dbModel = data[2].map( task => {
                task = task.task_assignment;
                const tempObject = {};
                tempObject.harvestId = task.id;
                tempObject.hourlyRate = task.hourly_rate;
                tempObject.billable = task.billable;
                tempObject.taskId = task.task_id;
                tempObject.payPeriodId = payPeriod.dataValues.id;
                return tempObject;
            });
            return repo.taskAssignment.CreateMany(dbModel);
        }).then( () => {
                const dayEntries = data[1].map( dayEntry => {
                    dayEntry = dayEntry.day_entry;
                    const tempObject = {};
                    tempObject.harvestId = dayEntry.id;
                    tempObject.notes = dayEntry.notes;
                    tempObject.hours = dayEntry.hours;
                    tempObject.userId = dayEntry.user_id;
                    tempObject.projectId = dayEntry.project_id;
                    tempObject.taskId = dayEntry.task_id;
                    tempObject.harvestUpdatedAt = dayEntry.updated_at;
                    tempObject.payPeriodId = payPeriodId;
                    return tempObject;
                });

                return repo.dayEntry.CreateMany(dayEntries);
        }). then( () => {
            payPeriodDbModel.id = payPeriodId;
            return new Promise( (resolve) => {
                        resolve(payPeriodDbModel);
            });
        });
        */

}