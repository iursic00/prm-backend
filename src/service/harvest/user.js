import request from '../../common/requests';
import harvestUrls from '../../configuration/urls';

function GetById(userId, accessToken) {
    return request.Get(harvestUrls.user.getById(userId, accessToken));
}

function GetAll(accessToken) {
    return request.Get(harvestUrls.user.getAll(accessToken));
}

export default {GetById, GetAll};