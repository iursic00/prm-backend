import harvestService from '../service/harvest';

class ProjectController {
    getByClientId(clientId, accessToken) {
        return harvestService.project.GetByClientId(clientId, accessToken);
    }
}

export default new ProjectController();