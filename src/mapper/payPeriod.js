import moment from 'moment';
import enums from '../configuration/enums';

function MapPayPeriodDbModelToPayPeriodDetailsScreen(dbModel) {
    return new Promise( (resolve) => {
        const viewModel = {};
        viewModel.id = dbModel.id;
        viewModel.name = dbModel.name;
        viewModel.startDate = dbModel.startDate;
        viewModel.endDate = dbModel.endDate;
        viewModel.price = dbModel.price;
        viewModel.status = dbModel.status;
        viewModel.clientId = dbModel.clientId;
        viewModel.taskAssignments = [];
        dbModel.taskAssignments.forEach( taskAssignment => {
            const tempObject = {};
            tempObject.id = taskAssignment.id;
            tempObject.title = taskAssignment.task.name;
            tempObject.hourlyRate = taskAssignment.hourlyRate;
            tempObject.description = taskAssignment.description;
            tempObject.taskId = taskAssignment.taskId;
            tempObject.billableHours = taskAssignment.billableHours;
            tempObject.unBillableHours = taskAssignment.unBillableHours;
            tempObject.comments = [];
            if(taskAssignment.billableHours === null || taskAssignment.unBillableHours === null) {
                tempObject.unBillableHours = 0;
                tempObject.billableHours = 0;
                taskAssignment.task.dayEntries.forEach( dayEntry => {
                    if(taskAssignment.billable) {
                        tempObject.billableHours += parseFloat(dayEntry.hours);
                    } else {
                        tempObject.unBillableHours += parseFloat(dayEntry.hours);
                    }
                });
            }
            tempObject.comments = taskAssignment.comments.map( comment => {
                const commentobj = {};
                commentobj.commentText = comment.commentText;
                commentobj.commentedBy = comment.commentBy == enums.Roles.Company.value ? 'Profico' : 'Client';
                commentobj.id = comment.id;
                commentobj.createdAt = moment(comment.createdAt).format('DD.MM.YYYY. HH:mm');
                return commentobj;
            });
            viewModel.taskAssignments.push(tempObject);
        });

        resolve(viewModel);
    });
}

export default {MapPayPeriodDbModelToPayPeriodDetailsScreen};