import repo from '../repository';
import mapper from '../mapper';

class DayEntryController {

    getByTaskIdAndPayPeriodId(payPeriodId, taskId) {
        return repo.dayEntry.GetByTaskIdAndPayPeriodId(payPeriodId, taskId)
            .then( dayEntries => {
                return mapper.dayEntry.MapDbDayEntriesArrayModelToViewModel(dayEntries);
            });
    }
}

export default new DayEntryController();