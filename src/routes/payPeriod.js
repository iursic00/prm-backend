import {Router} from 'express';
import PayPeriodController from '../controller/payPeriod';
import httpStatus from 'http-status-codes';
import moment from 'moment';

const router = Router();

router.post('/', (req, res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }
    if(!req.body.clientId || !req.body.projectId || !req.body.startDate || !req.body.endDate || !req.decoded.accessToken) {
        next();
    }
    if (!moment(req.body.startDate).isValid() ||!moment(req.body.endDate).isValid()) {
        return res.status(httpStatus.NOT_ACCEPTABLE).json({
            error: "Start date or end date is incorrect"
        });
    }

    PayPeriodController.Create(req.body.clientId, req.body.projectId, moment(req.body.startDate), moment(req.body.endDate), req.decoded.accessToken)
        .then( payPeriod => {
            return res.status(httpStatus.CREATED).json({
                payPeriod: payPeriod
            });
        })
        .catch( error => {
            error = error.message ? error.message : error;
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                message:error
            });
        });
});

router.get('/:year', (req,res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }
    const year = parseInt(req.params.year);
    if (!year) {
        next();
        return;
    }
    PayPeriodController.GetAllForHomePage(year)
        .then( (payPeriods) => {
            res.status(httpStatus.OK).json({
                payPeriods: payPeriods
            });
        })
        .catch( (error) => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                message: error
            });
        });
});

router.get( '/details/:id', (req, res, next) => {
    let id = parseInt(req.params.id);
    if(req.decoded.isClient) {
        id = req.decoded.payPeriodId;
    }
    if(isNaN(id) && req.decoded.isClient !== true ) {
        next();
        return;
    }

    PayPeriodController.GetPayPeriod(id)
        .then( payPeriod => {
            res.status(httpStatus.OK).json({
                data: payPeriod
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});

router.put('send-to-client', (req, res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }

    PayPeriodController.UpdateAndSendToClient(req.body)
        .then( assignemts => {
            res.status(httpStatus.OK).json({
                data: assignemts
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });


});

router.put( '/', (req, res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }

    if(isNaN(req.body.id)) {
        next();
    }
    PayPeriodController.Update(req.body)
        .then( assignemts => {
            res.status(httpStatus.OK).json({
                data: assignemts
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });

});

router.post('/internal', (req, res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }

    PayPeriodController.InternalFinished(req.body, req.decoded.accessToken)
        .then( payPeriod => {
            res.status(httpStatus.OK).json({
                data: payPeriod
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error.message
            });
        });
});

router.post('/client-reject', (req, res, next) => {
    if (req.decoded.isClient !== true) {
        next();
    }

    PayPeriodController.ClientReject(req.body)
        .then( payPeriod => {
            res.status(httpStatus.OK).json({
                data: payPeriod
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});

router.post('/client-accept', (req, res, next) => {
    if (req.decoded.isClient !== true) {
        next();
    }

    PayPeriodController.ClientAccept(req.body)
        .then( payPeriod => {
            res.status(httpStatus.OK).json({
                data: payPeriod
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});

router.delete( '/:id', (req, res, next) => {
    if (req.decoded.isAdmin !== true) {
        next();
    }

    PayPeriodController.Delete(req.params.id)
        .then( result => {
            res.status(httpStatus.OK).json({
                data: result
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error.message
            });
        });
});

router.all('*', (req,res) => {
    res.status(httpStatus.NOT_FOUND).json({
        message: "That route doesn't exist or parameters missing"
    });
});

export default router;