import db from '../models';

class CommentRepository {

    Create(comment) {
        return db.comment.create(comment);
    }

    DeleteByTaskAssignmetnIdArray(taskAssignmentIdArray) {
        return db.comment.destroy({
            where: {
                taskAssignmentId: taskAssignmentIdArray
            }
        });
    }
}

export default new CommentRepository();