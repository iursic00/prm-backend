import payPeriodMapper from './payPeriod';
import dayEntryMapper from './day-entry';

export default {
    payPeriod: payPeriodMapper,
    dayEntry: dayEntryMapper
};