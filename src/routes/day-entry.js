import {Router} from 'express';
import DayEntryController from '../controller/dayEntry';
import httpStatus from 'http-status-codes';

const router = Router();

router.get('/', (req, res, next) => {
    if(!req.query.payPeriodId || !req.query.taskId) {
        next();
        return;
    }
    DayEntryController.getByTaskIdAndPayPeriodId(parseInt(req.query.payPeriodId ), parseInt(req.query.taskId))
        .then( dayEntries => {
            res.status(httpStatus.OK).json({
                data: dayEntries
            });
        })
        .catch( error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                error: error
            });
        });
});
export default router;
