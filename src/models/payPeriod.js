
export default function(sequelize, DataTypes) {
    const PayPeriod = sequelize.define("payPeriod",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            name: {type : DataTypes.STRING, allowNull: false},
            startDate: {type: DataTypes.DATE, allowNull: false},
            endDate: {type: DataTypes.DATE, allowNull: false},
            price: {type: DataTypes.DECIMAL, allowNull: true},
            status: {type:DataTypes.INTEGER, allowNull:false},
            projectId: {type: DataTypes.INTEGER, allowNull: false},
            clientId: {type: DataTypes.INTEGER, allowNull: false},
            reasonForRejection: {type: DataTypes.TEXT, allowNull:true}
        },
        {
            indexes:[
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    PayPeriod.associate = function(models) {
        PayPeriod.taskAssignments = PayPeriod.hasMany(models.taskAssignment);
        PayPeriod.dayEntries = PayPeriod.hasMany(models.dayEntry);
    };

    return PayPeriod;
}