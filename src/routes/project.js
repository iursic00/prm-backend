import {Router} from 'express';
import ProjectController from '../controller/project';
import httpStatus from 'http-status-codes';

const router = Router();

router.get('/', (req, res, next) => {
   if(!req.query.clientId)
       next();

   ProjectController.getByClientId(req.query.clientId, req.decoded.accessToken)
       .then((projects) => {
           res.status(httpStatus.OK).json({
               projects:projects
           });
       })
       .catch((error) => {
           res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
               error: error
           });
       });
});

export default router;