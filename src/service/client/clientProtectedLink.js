import crypto from 'crypto';
import repo from '../../repository';

function create(payPeriod) {
    return new Promise( (resolve) => {
        const link = `${payPeriod.id}-${payPeriod.projectId}-${payPeriod.clientId}`;
        const cipher = crypto.createCipher('aes192', '905d65842c4e159baab0aa8de1350082e5b8686a9ad47296fca23ddc5ea58d2c');
        let encrypted = cipher.update(link, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        resolve(encrypted);
    });
}

function validate(link) {
    const decipher = crypto.createDecipher('aes192', '905d65842c4e159baab0aa8de1350082e5b8686a9ad47296fca23ddc5ea58d2c');
    let decrypted = "";
    let parameters = [];
    try{
        decrypted = decipher.update(link, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        parameters = decrypted.split('-');
    }catch(error) {
        throw new Error("Wrong link");
    }

    return repo.payPeriod.GetFromLinkParameters(parameters[0], parameters[1], parameters[2])
        .then( payPeriod => {
            return new Promise( (resolve, reject) => {
                if(payPeriod === null || !payPeriod || payPeriod.status == 1) {
                    reject("Wrong link");
                }
                resolve(payPeriod);
            });
        });

}

export default {validate, create};