
export default function(sequelize, DataTypes) {
    const DayEntry = sequelize.define("dayEntry",
        {
            id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey:true},
            harvestId: {type: DataTypes.INTEGER, allowNull: false, unique: true},
            notes: {type: DataTypes.TEXT, allowNull: true},
            hours: {type: DataTypes.DECIMAL, allowNull: false},
            userId: {type: DataTypes.INTEGER, allowNull: false},
            projectId: {type: DataTypes.INTEGER, allowNull: false},
            taskId: {type: DataTypes.INTEGER, allowNull: false},
            harvestUpdatedAt: {type: DataTypes.DATE, allowNull: true}
        },
        {
            indexes:[
                {
                    unique: true,
                    fields: ['id']
                }
            ]
        });

    DayEntry.associate = function(models) {

        DayEntry.belongsTo( models.task, {
            onDelete: "CASCADE",
            foreignKey: 'taskId',
            targetKey: 'harvestId'
        });

        DayEntry.belongsTo( models.user, {
            onDelete: "CASCADE",
            foreignKey: 'userId',
            targetKey: 'harvestId'
        });

        DayEntry.belongsTo( models.payPeriod, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull:false
            }
        });

    };

    return DayEntry;
}