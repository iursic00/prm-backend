import harvestService from '../harvest';
import repo from '../../repository';

function CreateTaskDbModel(task) {
    const temp = {};
    temp.harvestId = task.id;
    temp.name = task.name;
    temp.billableByDefault = task.billable_by_default;
    temp.harvestUpdatedAt = task.updated_at;
    temp.isDefault = task.is_default;
    temp.defaultHourlyRate = task.default_hourly_rate;
    temp.deactivated = task.deactivated;
    return temp;
}
export default function(accessToken) {
    const forCreation = [];
    const forUpdate = [];
    return Promise.all([
        harvestService.task.GetAll(accessToken),
        repo.task.GetAll()
    ])
    .then( data => {
        data[0].forEach( (harvestTask, harvestIndex) => {
           const index = data[1].findIndex( dbTask => {
               return dbTask.harvestId == harvestTask.task.id;
           });
           if (index == -1) {
               const temp = CreateTaskDbModel(data[0][harvestIndex].task);
               forCreation.push(temp);
           } else if (harvestTask.task.updated_at != data[1][index].harvestUpdatedAt) {
               const temp = data[1][index];
               temp.name = harvestTask.task.name;
               temp.billableByDefault = harvestTask.task.billable_by_default;
               temp.harvestUpdatedAt = harvestTask.task.updated_at;
               temp.isDefault = harvestTask.task.is_default;
               temp.defaultHourlyRate = harvestTask.task.default_hourly_rate;
               temp.deactivated = harvestTask.task.deactivated;
               forUpdate.push(temp.save());
           }
        });
      return Promise.all([repo.task.CreateMany(forCreation), Promise.all(forUpdate)]);
    });
}