import express from 'express';
import serverConfiguration from './configuration/server';
import createLogger from './common/logger';
import passport from './middleware/authorizationMiddleware';
import morgan from 'morgan';
import validateRequest from './middleware/validateRequest';
import bodyParser from 'body-parser';
import routes from './routes';
import cors from 'cors';
import models from './models';
import authorizeClient from './service/client/authorize';
import cluster from 'cluster';

const numberOfNodes = 1;
const app = express();
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());
global.logger = createLogger();
app.use(passport.initialize());

app.use('/api', validateRequest);
app.use('/api', routes);

app.get('/login', passport.authenticate('oauth2', {failureRedirect: '/error', session:false}), (req, res) => {
    res.redirect(`${serverConfiguration.frontendRedirectionUrl()}/login?token=${req.user.token}&isAdmin=${true}&name=${req.user.firstName} ${req.user.lastName}&avatarUrl=${req.user.avatarUrl}`);
});

app.get('/login/client/:id', (req, res) => authorizeClient(req, res));

app.get('/error', (req, res) => {
    res.redirect(`${serverConfiguration.frontendRedirectionUrl()}/login`);
});

if (cluster.isMaster) {
    for( let i = 0; i< numberOfNodes; i++) {
        cluster.fork();
    }

    cluster.on('online', (worker) => {
        logger.info('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', (worker, code, signal) => {
        logger.warn('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        logger.warn('Starting a new worker');
        cluster.fork();
    });

} else {
    models.sequelize.sync().then( () => {
        app.listen(serverConfiguration.port, (err) => {
            if (err) {
                logger.error(err);
                return;
            }
            logger.info(`Server listening on port ${serverConfiguration.port}`);
        });
    });
}