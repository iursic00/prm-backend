import jwt from 'jsonwebtoken';
import clientProtectedLinkService from './clientProtectedLink';
import serverConfiguration from '../../configuration/server';

function authorizeClient(req, res) {
    const id = req.params.id;
    clientProtectedLinkService.validate(id)
        .then( payPeriod => {
            const token = jwt.sign({
                isClient: true,
                payPeriodId: payPeriod.id ,
                projectId: payPeriod.projectId,
                clientId: payPeriod.clientId,
                clientName: "Client"
            }, serverConfiguration.jsonWebToken.secret);
            res.redirect(`${serverConfiguration.frontendRedirectionUrl()}/login?token=${token}&name=${"Client"}&isAdmin=false`);
        })
        .catch( error => {
            res.send(error.message);
        });

}

export default authorizeClient;