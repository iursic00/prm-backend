import Enum from 'enum';

const Roles = new Enum({
    Company: 1,
    Client: 2
});

const PayPeriodStatuses = new Enum({
    internalCreated: 1,
    internalConfirmed: 2,
    clientRejected: 3,
    clientAccepted: 4
});

export default {Roles, PayPeriodStatuses};