import repo from '../repository';
import harvestService from '../service/harvest';
import prepareAndCreateProject from '../service/project/prepareAndCreateProject';
import mapper from '../mapper';
import enums from '../configuration/enums';
import sendEmail from '../service/email/sendEmail';
import clientProtectedLinkService from '../service/client/clientProtectedLink';
import serverConfiguration from '../configuration/server';
import slackService from '../service/slack/sendMessage';

class PayPeriodController {
    Create(clientId, projectId, startDate, endDate, accessToken) {
        return repo.payPeriod.GetPayPeriodByProjectIdInTimeRange(projectId,startDate, endDate).then( payPeriods => {
            if (payPeriods && payPeriods.length > 0) {
                throw new Error("Can't create for that period because some dates are in other payPeriods");
            }
            return Promise.all([
                harvestService.project.GetById(projectId, accessToken),
                harvestService.entries.GetByProjectIdForPeriod(projectId, startDate.format('YYYYMMDD'), endDate.format('YYYYMMDD'), accessToken),
                harvestService.task.GetByProjectId(projectId, accessToken)
            ]);
            }).then( (data) => {
                return prepareAndCreateProject(data, startDate, endDate, projectId, clientId);
        });
    }

    GetAllForHomePage(year) {
        return repo.payPeriod.GetAllForYear(year);
    }

    GetPayPeriod(id) {
        return repo.payPeriod.GetPayPeriod(id)
            .then( payPeriod => {
                    return mapper.payPeriod.MapPayPeriodDbModelToPayPeriodDetailsScreen(payPeriod);
            });
    }

    Update(payPeriod) {
        const promiseArray = [];
        payPeriod.taskAssignments.forEach( taskAssignment => {
            promiseArray.push(repo.taskAssignment.UpdateOne(taskAssignment));
        });

        return Promise.all(promiseArray);
    }

    InternalFinished(payPeriod, accessToken) {
        let payPeriodTemp ={};
        let emailAdresses = [];
        return harvestService.client.GetContactsForClient(payPeriod.clientId, accessToken)
            .then( (contacts) => {
                if(contacts && contacts.length > 0) {
                    emailAdresses = contacts.map(contact => {
                        return contact.contact.email;
                    });
                }

                if (emailAdresses.length == 0) {
                    throw new Error("Please add contacts on Harvest for this client");
                }
                return repo.payPeriod.GetById(payPeriod.id);
            })
            .then( payPeriod => {
                if (payPeriod.status != enums.PayPeriodStatuses.internalCreated.value && payPeriod.status != enums.PayPeriodStatuses.clientRejected.value) {
                    throw new Error(`Only payPeriods with Internal created can put in internal finished`);
                }
                payPeriod.status = enums.PayPeriodStatuses.internalConfirmed.value;
                payPeriodTemp = payPeriod;
                return payPeriod.save();
            })
            .then( () => {
                const promiseArray = [];
                payPeriod.taskAssignments.forEach( taskAssignment => {
                    promiseArray.push(repo.taskAssignment.UpdateOne(taskAssignment));
                });
                return Promise.all(promiseArray);
            })
            .then( () => {
                return clientProtectedLinkService.create(payPeriodTemp);
            })
            .then( link => {
                return sendEmail(
                    emailAdresses.toString(),
                    `Pay Period Created - ${payPeriod.name}`,
                    `<html>
                        <div>Hello!</div>
                        </br>
                        <div>On the following link you can find ${payPeriod.name} WH report for the period ${payPeriod.startDate} - ${payPeriod.endDate}</div>
                        </br>
                        ${serverConfiguration.redirectionRootUrl()}/login/client/${link}
                        </br>
                        <div>Please verify report using the link provided, and feel free to leave any comments.</div>
                        </br>
                        <div>Thank you and kind regards,</div>
                        <div>Profico Team</div>
                      </html>`
                );
            });
    }

    ClientReject(payPeriod) {
        return repo.payPeriod.GetById(payPeriod.id)
            .then( payPeriodDb => {
                if (payPeriod.status != enums.PayPeriodStatuses.internalConfirmed.value) {
                    throw new Error(`Only payPeriods with Internal confirmed can go in internal finished`);
                }
                payPeriodDb.status = enums.PayPeriodStatuses.clientRejected.value;
                payPeriodDb.reasonForRejection = payPeriod.reasonForRejection;

                return payPeriodDb.save();
            })
            .then( () => {
                return sendEmail(
                    "iursic@profico.hr",
                    `Pay Period declined - ${payPeriod.name}`,
                    `<html>
                        </br>
                        <div>${payPeriod.name} WH report for period ${payPeriod.startDate} - ${payPeriod.endDate} <b>DECLINED</b>.</div>
                        </br>
                      </html>`
                );
            })
            .then( () => {
                return slackService.SendMessage(`${payPeriod.name} WH report for period ${payPeriod.startDate} - ${payPeriod.endDate} DECLINED. Reason: ${payPeriod.reasonForRejection}`);
            });
    }

    ClientAccept(payPeriod) {
        return repo.payPeriod.GetById(payPeriod.id)
            .then( payPeriod => {
                if (payPeriod.status != enums.PayPeriodStatuses.internalConfirmed.value) {
                    throw new Error(`Only payPeriods with Internal confirmed can put in internal finished`);
                }
                payPeriod.status = enums.PayPeriodStatuses.clientAccepted.value;

                return payPeriod.save();
            })
            .then( () => {
                return sendEmail(
                    "iursic@profico.hr",
                    `Pay Period accepted - ${payPeriod.name}`,
                    `<html>
                        </br>
                        <div>${payPeriod.name} WH report for period ${payPeriod.startDate} - ${payPeriod.endDate} <b>ACCEPTED</b>.</div>
                        </br>
                      </html>`
                );
            })
            .then( () => {
                return slackService.SendMessage(`${payPeriod.name} WH report for period ${payPeriod.startDate} - ${payPeriod.endDate} ACCEPTED`);
            });
    }

    Delete(payPeriodId) {
        let payPeriodTemp = null;
        return repo.payPeriod.GetById(payPeriodId)
            .then( payPeriod => {
                if (!payPeriod) {
                    throw new Error(`Pay period with ${payPeriod} doesn't exist`);
                }
                payPeriodTemp = payPeriod;
                return repo.payPeriod.GetById(payPeriodId);
            })
            .then( () => {
                return repo.taskAssignment.GetByPayPeriodId(payPeriodId);
            })
            .then( taskAssignments => {
                const ids = taskAssignments.map( taskAssignment => {
                    return taskAssignment.id;
                });

                return repo.comment.DeleteByTaskAssignmetnIdArray(ids);
            })
            .then( () => {
                return repo.dayEntry.DeleteByPayPeriodId(payPeriodId);
            })
            .then( () => {
                return repo.taskAssignment.DeleteByPayPeriodId(payPeriodId);
            })
            .then( () => {
                return payPeriodTemp.destroy();
            });
    }
}

export default new PayPeriodController();