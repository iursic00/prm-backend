import harvestService from '../harvest';
import repo from '../../repository';

function CreateUserDbModel(user) {
    const temp = {};
    temp.harvestId = user.id;
    temp.isAdmin = user.is_admin;
    temp.isProjectManager = user.is_project_manager;
    temp.firstName = user.first_name;
    temp.lastName = user.last_name;
    temp.isContractor = user.is_contractor;
    temp.isActive = user.is_active;
    temp.email = user.email;
    temp.harvestUpdatedAt = user.updated_at;
    return temp;
}
export default function(accessToken) {
    const forCreation = [];
    const forUpdate = [];
    return Promise.all([
        harvestService.user.GetAll(accessToken),
        repo.user.GetAll()
    ])
    .then( data => {
        data[0].forEach( (harvestUser, harvestIndex) => {
            const index = data[1].findIndex( dbUser => {
                return dbUser.harvestId == harvestUser.user.id;
            });
            if (index == -1) {
                const temp = CreateUserDbModel(data[0][harvestIndex].user);
                forCreation.push(temp);
            } else if (harvestUser.user.updated_at != data[1][index].harvestUpdatedAt) {
                const temp = data[1][index];
                temp.harvestId = harvestUser.user.id;
                temp.isAdmin = harvestUser.user.is_admin;
                temp.isProjectManager = harvestUser.user.is_project_manager;
                temp.firstName = harvestUser.user.first_name;
                temp.lastName = harvestUser.user.last_name;
                temp.isContractor = harvestUser.user.is_contractor;
                temp.isActive = harvestUser.user.is_active;
                temp.email = harvestUser.user.email;
                temp.harvestUpdatedAt = harvestUser.user.updated_at;
                forUpdate.push(temp.save());
            }
        });
        return Promise.all([repo.user.CreateMany(forCreation), Promise.all(forUpdate)]);
    });
}