#!/usr/bin/env bash
pwd
echo "$USER"
groups jenkins
docker rmi -f prm-backend
docker build -t prm-backend .
docker run --net="host" -p 2907:2907 prm-backend