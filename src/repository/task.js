import db from '../models';

class TaskRepository {

    CreateMany(tasks) {
        return db.task.bulkCreate(tasks);
    }

    GetAll() {
        return db.task.findAll();
    }
}

export default new TaskRepository();