import request from '../../common/requests';
import harvestUrls from '../../configuration/urls';

function GetById(taskId, accessToken) {
    return request.Get(harvestUrls.task.getById(taskId, accessToken));
}

function GetByProjectId(projectId, accessToken) {
    return request.Get(harvestUrls.task.getByProjectId(projectId, accessToken));
}

function GetAll(accessToken) {
    return request.Get(harvestUrls.task.getAll(accessToken));
}
export default {GetById, GetByProjectId, GetAll};